function car_sort(inventory)
{
    if (!Array.isArray(inventory))
    {
        throw Error("invalid argument passed");
    }
    let n = inventory.length;
    for (let i = 0; i < n; i++)
    {
        for (let j = i + 1 ; j < n; j++)
        {
            if (inventory[i].car_model.toLowerCase() > inventory[j].car_model.toLowerCase())
            {
                let temp = inventory[i]
                inventory[i] = inventory[j];
                inventory[j] = temp;
            }
        }
    }
    return inventory;
};
export {car_sort};