import {inventory} from "./car_inventory.js";
function lastCarInfo(inventory)
{
    if (!Array.isArray(inventory))
    {
        throw Error("invalid arguments passed");
    }
    let index = inventory.length - 1;
    return `Last car is a ${inventory[index].car_make} ${inventory[index].car_model}`
}
export {lastCarInfo};