function car_info(inventory,id){
    if (!Array.isArray(inventory) ||  typeof(id) !== "number")
    {
        throw Error("invalid arguments");
    }
    for (let i = 0; i < inventory.length; i++)
    {
        if ( inventory[i].id === id)
        {
            return `Car ${id} is a ${inventory[i].car_year} ${inventory[i].car_make} ${inventory[i].car_model}`;
        }
    }
}
export {car_info};