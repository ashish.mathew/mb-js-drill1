// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.
import { get_BMWandAUDI } from "../problem6.js";
import { inventory } from "../car_inventory.js";

try {
    console.log(JSON.stringify(get_BMWandAUDI(inventory)));
} catch (error) {
    console.log(error);
}