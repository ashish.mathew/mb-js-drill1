// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
import { inventory } from "../car_inventory.js";
import { car_sort } from "../problem3.js";

try {
    console.log(car_sort(inventory)); 
} catch (error) {
    console.log(error);
}