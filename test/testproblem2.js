// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of: 
import { inventory } from "../car_inventory.js";
import { lastCarInfo } from "../problem2.js";

try {
    console.log(lastCarInfo({}));
} catch (error) {
    console.log(error);
}