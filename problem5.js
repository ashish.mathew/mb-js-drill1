function find_oldcars(caryears)
{
    if(!Array.isArray(caryears))
    {
        throw Error("invalid argument");
    }
    let n = caryears.length;
    let oldcars = [];
    for (let i = 0; i < n; i++)
    {
        if (caryears[i] < 2000)
        {
            oldcars.push(caryears[i]);
        }
    }
    return oldcars;
};
export {find_oldcars};