function get_BMWandAUDI(inventory)
{
    if(!Array.isArray(inventory))
    {
        throw Error("invalid arguments");
    }
    let cust_cars = [];
    let n = inventory.length;
    for (let i = 0; i < n; i++)
    {
        if (inventory[i].car_make === "BMW" || inventory[i].car_make === "Audi")
        {
            cust_cars.push(inventory[i]);
        }

    }
    return cust_cars;
};
export {get_BMWandAUDI};