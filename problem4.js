function car_years(inventory){
    if (!Array.isArray(inventory))
    {
        throw Error("invalid argument passed");
    }
    let years = [];
    let n = inventory.length;
    for (let i = 0; i < n; i++)
    {
        years.push(inventory[i].car_year);
    }
    return years;
}
export { car_years};
